﻿using API.Mapping;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.App_Start
{
    public class AutoMapperConfig
    {
        public static void Initialize()
        {
            Mapper.Initialize((config) =>
            {
                config.AddProfile(new SnowboardsMappingProfile());
                config.AddProfile(new FirmsMappingProfile());
            });
        }
    }
}