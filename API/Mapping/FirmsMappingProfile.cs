﻿using API.Models;
using API.Resources;
using API.ViewModels;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Mapping
{
    public class FirmsMappingProfile : Profile
    {
        public FirmsMappingProfile()
        {
            this.CreateMap<Firm, KeyValuePairRes>()
                .ForMember(des => des.Key, opt => opt.MapFrom(src => src.Id))
                .ForMember(des => des.Value, opt => opt.MapFrom(src => src.Name));
        }
    }
}