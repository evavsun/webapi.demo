﻿using API.Models;
using API.Resources;
using API.ViewModels;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Mapping
{
    public class SnowboardsMappingProfile : Profile
    {
        public SnowboardsMappingProfile()
        {
            this.CreateMap<Snowboard, SnowboardDetailsRes>()
                .ForMember(des => des.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(des => des.Type, opt => opt.MapFrom(src => src.Type))
                .ForMember(des => des.Color, opt => opt.MapFrom(src => src.Color))
                .ForMember(des => des.Color, opt => opt.MapFrom(src => src.Color))
                .ForMember(des => des.FirmName, opt => opt.MapFrom(src => src.Firm.Name));

            this.CreateMap<SnowboardRes, Snowboard>()
                .ForMember(des => des.Type, opt => opt.MapFrom(src => src.Type))
                .ForMember(des => des.Color, opt => opt.MapFrom(src => src.Color))
                .ForMember(des => des.FirmId, opt => opt.MapFrom(src => src.FirmId));
        }
    }
}