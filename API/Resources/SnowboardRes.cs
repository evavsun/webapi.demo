﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Resources
{
    public class SnowboardRes
    {
        public string Type { get; set; }

        public string Color { get; set; }

        public int FirmId { get; set; }
    }
}