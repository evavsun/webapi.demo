﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Resources
{
    public class KeyValuePairRes
    {
        public int? Key { get; set; }

        public string Value { get; set; }

        public string Metadata { get; set; }
    }
}