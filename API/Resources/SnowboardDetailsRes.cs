﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.ViewModels
{
    public class SnowboardDetailsRes
    {
        public int Id { get; set; }

        public string Type { get; set; }

        public string Color { get; set; }

        public string FirmName { get; set; }
    }
}