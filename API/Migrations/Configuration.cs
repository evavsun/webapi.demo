﻿namespace API.Migrations
{
    using API.Models;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<API.Context.SportshopContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = false;
        }

        protected override void Seed(API.Context.SportshopContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            if(!context.Firms.Any())
            {
                List<Firm> companies = new List<Firm>
                {
                    new Firm { Name = "Burton" },
                    new Firm { Name = "Head" },
                    new Firm { Name = "Capita" }
                };

                context.Firms.AddRange(companies);
                context.SaveChanges();
            }

            if(!context.Snowboards.Any())
            {
                var companies = context.Firms.ToList();

                List<Snowboard> snowboards = new List<Snowboard>
                {
                    new Snowboard { Type = "Rocker", Color = "#7CFC00", FirmId = companies[0].Id },
                    new Snowboard { Type = "Camber", Color = "#4B0082", FirmId = companies[1].Id },
                    new Snowboard { Type = "Hybrid", Color = "#FFF8DC", FirmId = companies[2].Id },
                    new Snowboard { Type = "Banana", Color = "#FF1493", FirmId = companies[2].Id },
                };

                context.Snowboards.AddRange(snowboards);
                context.SaveChanges();
            }
        }
    }
}
