namespace API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Firms",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Snowboards",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(),
                        Color = c.String(),
                        FirmId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Firms", t => t.FirmId)
                .Index(t => t.FirmId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Snowboards", "FirmId", "dbo.Firms");
            DropIndex("dbo.Snowboards", new[] { "FirmId" });
            DropTable("dbo.Snowboards");
            DropTable("dbo.Firms");
        }
    }
}
