﻿using API.Models;
using System.Data.Entity;


namespace API.Context
{
    public class SportshopContext : DbContext
    {
        public SportshopContext()
            : base("DefaultConnection")
        {

        }

        public DbSet<Firm> Firms { get; set; }

        public DbSet<Snowboard> Snowboards { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Snowboard>()
                .HasOptional<Firm>(f => f.Firm)
                .WithMany(f => f.Snowboards)
                .HasForeignKey(f => f.FirmId);
        }
    }
}