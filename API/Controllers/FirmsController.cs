﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using API.Context;
using API.Models;
using API.Resources;
using AutoMapper;

namespace API.Controllers
{
    [RoutePrefix("api/firms")]
    public class FirmsController : ApiController
    {
        private SportshopContext db = new SportshopContext();

        // GET: api/Сompany
        [HttpGet]
        [ResponseType(typeof(IEnumerable<Firm>))]
        [Route("list")]
        public async Task<IEnumerable<Firm>> GetСompanies()
        {
            var companies = await db.Firms.ToListAsync();
            return companies;
        }

        [HttpGet]
        [ResponseType(typeof(List<KeyValuePairRes>))]
        [Route("dropdown")]
        public async Task<List<KeyValuePairRes>> GetDropdownCompanies()
        {
            var firms = await db.Firms.ToListAsync();
            var keyValue = Mapper.Map<List<Firm>, List<KeyValuePairRes>>(firms);
            return keyValue;
        }

        // GET: api/Сompany/5
        [ResponseType(typeof(Firm))]
        [Route("getById/{id}")]
        public async Task<IHttpActionResult> GetСompany(int id)
        {
            Firm сompany = await db.Firms.FindAsync(id);
            if (сompany == null)
            {
                return NotFound();
            }

            return Ok(сompany);
        }

        [ResponseType(typeof(Firm))]
        [Route("getByName/{name}")]
        public async Task<IHttpActionResult> GetСompanyByName(string name)
        {
            Firm сompany = await db.Firms.FirstOrDefaultAsync(f => f.Name == name);
            if (сompany == null)
            {
                return NotFound();
            }

            return Ok(сompany);
        }

        // PUT: api/Сompany/5
        [ResponseType(typeof(Firm))]
        public async Task<IHttpActionResult> PutСompany(int id, Firm сompany)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != сompany.Id)
            {
                return BadRequest();
            }

            db.Entry(сompany).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!СompanyExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Сompany
        [ResponseType(typeof(Firm))]
        public async Task<IHttpActionResult> PostСompany(Firm сompany)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Firms.Add(сompany);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = сompany.Id }, сompany);
        }

        // DELETE: api/Сompany/5
        [ResponseType(typeof(Firm))]
        // [Route("bolt/bolt/api/api/{id}")]
        public async Task<IHttpActionResult> DeleteСompany(int id)
        {
            Firm сompany = await db.Firms.FindAsync(id);
            if (сompany == null)
            {
                return NotFound();
            }

            db.Firms.Remove(сompany);
            await db.SaveChangesAsync();

            return Ok(сompany);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool СompanyExists(int id)
        {
            return db.Firms.Count(e => e.Id == id) > 0;
        }
    }
}