﻿using API.Context;
using API.Services;
using System;
using System.Web.Http;


namespace API.Controllers
{
    public class TestController : ApiController
    {
        private readonly IMessageProvider messageProvider;

        public TestController(IMessageProvider messageProvider)
        {
            this.messageProvider = messageProvider;
        }

        // GET api/values
        public IHttpActionResult Get()
        {
            var request = Request;
            // var x = HttpContext.Current.Request.UrlReferrer;
            // HttpResponseMessage
            // var response = Request.CreateResponse<IEnumerable<string>>(HttpStatusCode.OK, new string[] { "value1", "value2" });
            return Ok(new string[] { "value1", "value2" });
        }

        // GET api/values/5
        [HttpGet]
        [Route("api/test/author/{id}")]
        public string Get(int id)
        {
            return "value";
        }

        [HttpGet]
        [Route("api/test/book/{id}")]
        public string GetById(int id)
        {
            return "";
        }

        [HttpPost]
        public IHttpActionResult Post([FromBody]string value)
        {
            this.messageProvider.Send();

            var context = new SportshopContext();


            return Redirect(new Uri("https://docs.microsoft.com/en-us/aspnet/web-api/overview/getting-started-with-aspnet-web-api/tutorial-your-first-web-api"));
        }

        [HttpPut]
        public void Put(int id, [FromBody]string value)
        {
        }

        [HttpDelete]
        public void Delete(int id)
        {
        }
    }
}
