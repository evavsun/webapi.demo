﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using API.Context;
using API.Models;
using API.Resources;
using API.ViewModels;
using AutoMapper;

namespace API.Controllers
{
    [RoutePrefix("api/snowboards")]
    public class SnowboardsController : ApiController
    {
        private SportshopContext db = new SportshopContext();

        // GET: api/Snowboards
        [Route("list")]
        [ResponseType(typeof(IEnumerable<SnowboardDetailsRes>))]
        public async Task<IHttpActionResult> GetSnowboards()
        {
            Uri myReferrer = HttpContext.Current.Request.UrlReferrer; // CORS
            var snowboards = await db.Snowboards.ToListAsync();
            var snowboardRes = Mapper.Map<List<Snowboard>, List<SnowboardDetailsRes>>(snowboards);
            return Ok(snowboardRes);
        }

        // GET: api/Snowboards/5
        [Route("getById/{id}")]
        [ResponseType(typeof(Snowboard))]
        public async Task<IHttpActionResult> GetSnowboard(int id)
        {
            Snowboard snowboard = await db.Snowboards.FindAsync(id);
            if (snowboard == null)
            {
                return NotFound();
            }

            return Ok(snowboard);
        }

        // PUT: api/Snowboards/5
        [ResponseType(typeof(Snowboard))]
        [HttpPut]
        public async Task<IHttpActionResult> Update(int id, Snowboard snowboard)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != snowboard.Id)
            {
                return BadRequest();
            }

            db.Entry(snowboard).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SnowboardExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Snowboards
        [ResponseType(typeof(SnowboardRes))]
        [HttpPost]
        public async Task<IHttpActionResult> CreateSnowboard(SnowboardRes snowboard)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var newBoard = Mapper.Map<SnowboardRes, Snowboard>(snowboard);

            db.Snowboards.Add(newBoard);

            await db.SaveChangesAsync();

            return Ok();
        }

        // DELETE: api/Snowboards/5
        [ResponseType(typeof(Snowboard))]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int id)
        {
            Snowboard snowboard = await db.Snowboards.FindAsync(id);
            if (snowboard == null)
            {
                return NotFound();
            }

            db.Snowboards.Remove(snowboard);
            await db.SaveChangesAsync();

            return Ok(snowboard);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SnowboardExists(int id)
        {
            return db.Snowboards.Count(e => e.Id == id) > 0;
        }
    }
}