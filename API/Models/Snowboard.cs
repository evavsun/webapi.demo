﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class Snowboard
    {
        public int Id { get; set; }

        public string Type { get; set; }

        public string Color { get; set; }

        public int? FirmId { get; set; }

        public virtual Firm Firm { get; set; }
    }
}