﻿using System.Collections.Generic;

namespace API.Models
{
    public class Firm
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<Snowboard> Snowboards { get; set; }
    }
}